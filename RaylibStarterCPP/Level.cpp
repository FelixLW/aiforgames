#pragma once
#include "raygui.h"
#include "Level.h"

Level::Level(int screenWidth, int screenHeight)
    : m_screenWidth(screenWidth), m_screenHeight(screenHeight)
{

}

Level::~Level()
{

}

bool Level::Startup()
{
    InitWindow(m_screenWidth, m_screenHeight, "Stealth Game");
    SetTargetFPS(60);

	return true;
}

void Level::Shutdown()
{
    CloseWindow();
}

void Level::Update()
{
    
}

void Level::Draw()
{
    

    
}

bool Level::IsRunning()
{
    return !WindowShouldClose();
}
