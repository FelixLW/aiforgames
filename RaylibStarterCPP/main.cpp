#pragma once
#include "Level.h"

int main(int argc, char* argv[])
{
    Level level;

    // Initialization
    //-------------------------------------------------------------------------------------- 
    level.Startup();   

    Rectangle scissorArea = { 0, 0, 300, 300 };
    bool scissorMode = true;
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (level.IsRunning())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        //level.Update();
        if (IsKeyPressed(KEY_S))
            scissorMode = !scissorMode;

        // Centre the scissor area around the mouse position
        scissorArea.x = GetMouseX() - scissorArea.width / 2;
        scissorArea.y = GetMouseY() - scissorArea.height / 2;
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        //level.Draw();

        BeginDrawing();

        ClearBackground(RAYWHITE);

        if (scissorMode) BeginScissorMode(scissorArea.x, scissorArea.y, scissorArea.width, scissorArea.height);

        // Draw full screen rectangle and some text
        // NOTE: Only part defined by scissor area will be rendered
        DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), RED);
        DrawText("Move the mouse around to reveal this text!", 190, 200, 20, LIGHTGRAY);

        if (scissorMode) EndScissorMode();

        DrawRectangleLinesEx(scissorArea, 1, BLACK);
        DrawText("Press S to toggle scissor test", 10, 10, 20, BLACK);
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------   
    level.Shutdown();
    //--------------------------------------------------------------------------------------

    return 0;
}