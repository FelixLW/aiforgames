#pragma once
#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raylib.h"


//struct Scissor
//{	
//	bool ScissorMode = true;
//};

class Level
{
public:
	Level(int screenWidth = 1280, int screenHeight = 720);
	~Level();

	bool Startup();
	void Shutdown();
	void Update();
	void Draw();	

	bool IsRunning();
	

protected:
	int m_screenWidth;
	int m_screenHeight;

	//Scissor* s;
	//Rectangle ScissorArea = { 0, 0, 300, 300 };
};

