#include "State.h"
#include "Transition.h"

State::~State()
{
}

void State::AddTransition(Transition* transition)
{
	m_transitions.push_back(transition);
}

Transition* State::GetTriggeredTransition(Agent* agent)
{
	for (auto transition : m_transitions)
	{
		if (transition->HasTriggered(agent))
		{
			return transition;
		}
	}
	return nullptr;
}

void IdleState::Init(Agent* agent)
{
	agent->SetData<bool>("attackEnabled", false);
	agent->SetData<Color>("colour", DARKGREEN);
}

void IdleState::Update(Agent* agent, float deltaTime)
{
	std::cout << "idle state" << std::endl;
}

void AttackState::Init(Agent* agent)
{
	agent->SetData<bool>("attackEnabled", true);
	agent->SetData<Color>("colour", RED);
}

void AttackState::Update(Agent* agent, float deltaTime)
{
	std::cout << "attack state" << std::endl;
}

void WanderState::Init(Agent* agent)
{
	agent->SetData<bool>("attackEnabled", false);
	agent->SetData<Color>("colour", DARKGREEN);
}

void WanderState::Update(Agent* agent, float deltaTime)
{
	std::cout << "wander state" << std::endl;
}
