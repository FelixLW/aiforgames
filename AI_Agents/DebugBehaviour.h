#pragma once
#include "SteeringForce.h"
#include "Agent.h"

class DebugBehaviour :
	public SteeringForce
{
	virtual Vector2 Update(Agent* agent, float deltaTime) override;
	void Draw(Agent* agent) override;
};

