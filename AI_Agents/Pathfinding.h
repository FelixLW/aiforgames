#pragma once
#pragma warning(push)
#pragma warning(disable: 26451)
#include "raylib.h"
#pragma warning(pop)
#include <vector>
#include <algorithm>
#include <string>

enum class TileType
{
	GROUND = 1,
	NONE = 0,	
	WALL = -1	

};

typedef std::vector<std::vector<TileType>> TileMap;

struct Node;

struct Edge
{
	Node* target;
	float cost;
};

struct Node
{
	Node();
	Node(const Vector2& pos);
	Node(const unsigned int id, const Vector2& pos, TileType tileType = TileType::NONE);
	Node(const std::string& id, const Vector2& pos, TileType tileType = TileType::NONE);

	void AddConnection(Node* other, float cost = 1, bool addZeroCostConnections = true)
	{
		if (addZeroCostConnections || (cost > 0))
			connections.push_back({ other, cost });
	}

	//Gives nodes an id
	std::string id = "";

	TileType tileType = TileType::NONE;

	Vector2 position = { 0,0 };

	float gScore = 0;	//cost to travel from starting node to the node currently being traversed
	float hScore = 0;	//for each node we calculate how far away we think it is from the goal - how far to destination
	float fScore = 0;	//final score for a node is the sum of g and h scores - gscore + hscore
	Node* parent = nullptr;

	std::vector<Edge> connections;
};

typedef std::vector<Node*> Path;

//responsible for containing a list of nodes
class NodeGraph
{
public:

	Path AStarSearch(Node* startNode, Node* endNode);

	Path DijkstraSearch(Node* startNode, Node* endNode);
	Path DijkstraSearchID(const std::string& startNodeId, const std::string& endNodeID);

	void DrawNode(Node* node, bool selected = false);
	void DrawGraph(Node* node, std::vector<Node*>* drawnList);

	virtual void Draw();

	void AddNode(Node* node);
	void DrawNode();

	Node* FindNodeId(const std::string& id);

	void ConnectNodes(const std::string& NodeAId, const std::string& NodeBId, bool isTwoWay);
	void ConnectNodes(const std::string& NodeAId, const std::string& NodeBId, bool isTwoWay, float cost);

	Vector2 GetPositionByNodeId(const std::string& NodeId);

	void DrawEdges();

	void DrawNodes();

	void DrawNodeIds();

	//finds the node closest to the position we pass in
	Node* FindClosestNode(const Vector2& pos);

	Node* GetNode(unsigned int index);

	//returns the list of nodes 
	//data only gets updated through NodeGraph2D, but m_nodeList somehow always returns 0
	std::vector<Node*>& GetList() { return m_nodeList; }

protected:
	std::vector<Node*> m_nodeList;
};

class NodeGraph2D
	: public NodeGraph
{
public:
	//creates a nodeGraph from an array of tiles
	void CreateGraph(const TileMap& tiles, unsigned int offsetX = 0, unsigned int offsetY = 0, unsigned int tileSizeX = 32, unsigned int tileSizeY = 32);

	Node* GetNode(unsigned int x, unsigned int y);

protected:
	//sub classes can access this function when pathfinding
	virtual float GetCost(const TileType tileType);

protected:
	unsigned int m_Width = 0;
	unsigned int m_Height = 0;
	unsigned int m_tileSizeX = 0;
	unsigned int m_tileSizeY = 0;
};