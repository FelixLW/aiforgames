#pragma once
#include "Behaviour.h"
#include <vector>

class Condition
	: public Behaviour
{
public:
	Condition() {}
	virtual ~Condition() {}

	virtual bool Test(Agent* agent) const = 0;

	virtual eBehaviourResult Update(Agent* agent, float deltaTime) {
		if (Test(agent))
			return eBehaviourResult::SUCCESS;
		return eBehaviourResult::FAILURE;
	}
};

class WithinRangeCondition
	: public Condition
{
public:
	WithinRangeCondition(Agent* agent, Agent* target, float range)
		: m_target(target), m_range(range) {}


	virtual ~WithinRangeCondition() {}

	virtual bool Test(Agent* agent) const;

private:
	Agent*	m_target;
	float	m_range;
};

class OutOfRangeCondition
	: public Condition
{
public:
	OutOfRangeCondition(Agent* agent, Agent* target, int range)
		: m_target(target)
	{
		//agent->SetData<int>("patrolRange", range);
	}

	virtual ~OutOfRangeCondition() {}

	virtual bool Test(Agent* agent) const;

private:
	Agent* m_target;
};