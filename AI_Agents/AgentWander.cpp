#include "AgentWander.h"

AgentWander::AgentWander()
{
}

AgentWander::~AgentWander()
{
}

// Draw the agent
void AgentWander::Draw(Color colour)
{
	DrawCircle(m_position.x, m_position.y, 10, colour);
}