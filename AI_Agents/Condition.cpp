#include "Condition.h"

bool WithinRangeCondition::Test(Agent* agent) const
{
    //get the target's position
    Vector2 targetPosition = m_target->GetPosition();   //this won't work as the left of the screen has smaller vlaues than right

    //get this agent's position
    Vector2 ourPosition = agent->GetPosition();

    //compare the two positions
    Vector2 positionDifference = Vector2Subtract(targetPosition, ourPosition);

    float distance = sqrtf(positionDifference.x * positionDifference.x + positionDifference.y * positionDifference.y);

    return distance <= 200;
}

bool OutOfRangeCondition::Test(Agent* agent) const
{
    //get the target's position
    Vector2 targetPosition = m_target->GetPosition();

    //get this agent's position
    Vector2 ourPosition = agent->GetPosition();

    //compare the two positions
    Vector2 positionDifference = Vector2Subtract(targetPosition, ourPosition);

    float distance = sqrtf(positionDifference.x * positionDifference.x + positionDifference.y * positionDifference.y);

    return distance > 200;
}
