#include "AttackForce.h"

Vector2 AttackForce::Update(Agent* agent, float deltaTime)
{
    if (agent->GetData<bool>("attackEnabled") == true)
    {
        //get the distance between target and enemy agent
        Vector2 distanceBetweenPoint = Vector2Subtract(m_target->GetPosition(), agent->GetPosition());

        if (distanceBetweenPoint.x == 0 && distanceBetweenPoint.y == 0) {
            return { 0, 0 };
        }
        else
        {
            Vector2 scaledVeloctiy = Vector2Scale(Vector2Normalize(distanceBetweenPoint), agent->GetMaxSpeed());
            Vector2 steeringForce = Vector2Subtract(scaledVeloctiy, agent->GetVelocity()); 

            return steeringForce;
        }
    }
    else
    {
        return { 0,0 };
    }

    /*Vector2 distanceBetweenPoint = Vector2Subtract(m_target->GetPosition(), agent->GetPosition());

    if (distanceBetweenPoint.x == 0 && distanceBetweenPoint.y == 0) {
        return { 0, 0 };
    }
    else
    {
        Vector2 scaledVeloctiy = Vector2Scale(Vector2Normalize(distanceBetweenPoint), agent->GetMaxSpeed());
        Vector2 steeringForce = Vector2Subtract(scaledVeloctiy, agent->GetVelocity());

        return steeringForce;
    }*/
}
