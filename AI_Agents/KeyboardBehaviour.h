#pragma once
#include "Agent.h"
#include "SteeringForce.h"

class KeyboardBehaviour :
	public SteeringForce
{
public:
	KeyboardBehaviour(float speed):
	m_speedIncrement(speed)
	{};
	virtual ~KeyboardBehaviour() {};

	virtual Vector2 Update(Agent* agent, float deltaTime);

private:
	float m_speedIncrement = 0;
};

