#include "SteeringBehaviour.h"
#include "SteeringForce.h"

SteeringBehaviour::SteeringBehaviour()
{
}

SteeringBehaviour::~SteeringBehaviour()
{
	for (SteeringForce* steeringForce : m_steeringForces)
	{
		delete steeringForce;
	}
}

Behaviour::eBehaviourResult SteeringBehaviour::Update(Agent* agent, float deltaTime)
{

	Vector2 steeringAcceleration = { 0, 0 };
	Vector2 totalAcceleration = { 0,0 };

	for (SteeringForce* steeringForce : m_steeringForces)
	{
		//retrieve the steering forces from each steeringForces
		steeringAcceleration = steeringForce->Update(agent, deltaTime);

		//muliply according to weighting 
		steeringAcceleration = Vector2Scale(steeringAcceleration, steeringForce->GetWeighting()); //

		//truncate the sum of all desired accelerations
		totalAcceleration = Truncate(Vector2Add(totalAcceleration, steeringAcceleration), m_maxAcceleration);
	}

	//apply the acceleration
	Vector2 velocityChange = Vector2Scale(totalAcceleration, deltaTime);

	//update the agent's velocity
	agent->SetVelocity(Vector2Add(agent->GetVelocity(), velocityChange));

	//return the data type eBehaviour as success 
	return eBehaviourResult::SUCCESS;
}

void SteeringBehaviour::Draw(Agent* agent)
{
	for (SteeringForce* steeringForce : m_steeringForces)
	{
		steeringForce->Draw(agent);
	}
}

void SteeringBehaviour::AddSteeringForce(SteeringForce* steeringForce)
{
	m_steeringForces.push_back(steeringForce);
}

Vector2 SteeringBehaviour::Truncate(Vector2 v, float max)
{
	float i = max / Vector2Length(v);
	i = (float)(i < 1.0 ? i : 1.0);
	return Vector2Scale(v, i);
}

