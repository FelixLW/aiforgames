#pragma once
#include "raymath.h"
#include "Blackboard.h"
#include <vector>

class Behaviour;

class Agent
	: public BlackboardCollection
{
public:
	Agent();
	Agent(float speed, Color colour);
	virtual ~Agent();

	// Update the agent and its behaviours
	virtual void Update(float deltaTime);

	virtual void Draw();

	void AddBehaviour(Behaviour* behaviour);

	void SetPosition(Vector2 position) { m_position = position; }
	Vector2 GetPosition() { return m_position; }

	void SetVelocity(Vector2 velocity) { m_velocity = velocity; }
	Vector2 GetVelocity() { return m_velocity; }

	virtual void SetMaxSpeed(float speed) { m_maxSpeed = speed; }
	virtual float GetMaxSpeed() { return m_maxSpeed; }

	void OffScreen(Agent* agent, int sw, int sh);

private:
	Vector2 Truncate(Vector2 v, float max);

protected:
	std::vector<Behaviour*> m_behaviourList;

	Vector2 m_position = { 0, 0 };
	Vector2 m_velocity = { 0, 0 };
	float m_maxSpeed = 100;

	int m_width = 0;
	int m_height = 0;
};

