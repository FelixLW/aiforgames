#pragma once
#include "Agent.h"

class AgentWander :
	public Agent
{
public:
	AgentWander();
	virtual ~AgentWander();

	// Draw the agent
	virtual void Draw(Color colour) override;

	virtual void SetMaxSpeed(float speed) override { m_maxSpeed = speed; }
	virtual float GetMaxSpeed() override { return m_maxSpeed; }

	virtual void SetColour(Color colour) override { m_colour = colour; }
	virtual Color GetColour() override { return m_colour; }

protected:

	float m_maxSpeed = 60;
	Color m_colour = PINK;
};

