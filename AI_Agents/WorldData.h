#pragma once
#include "Pathfinding.h"

class WorldData :
    public NodeGraph2D
{
public:
    WorldData();
    WorldData(const TileMap& tileMap);
    WorldData(const TileMap& tileMap, unsigned int offsetX, unsigned int offsetY, unsigned int tileSizeX, unsigned int tileSizeY);

    void Draw(Texture2D wall);

    unsigned int TileMapSize()
    {
        //returns the full vector size
        int tileMapSize = 0;
        for (unsigned int i = 0; i < tileMap.size(); i++)
        {
            tileMapSize += tileMap[i].size();
        }

        if (tileMapSize == 0)
            throw "tileMapSize is 0. Error calculating actual size";

        //it does give us a total amount of tiles/nodes being used, but we 
        //minus one since node/tile[0] is counted, if we we're to use the max number, it would be out of bounds
        tileMapSize -= 1;

        return tileMapSize;
    }

protected:
    TileMap tileMap;
};

