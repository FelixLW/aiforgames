#include "Agent.h"
#include "Behaviour.h"
#include <algorithm>

Agent::Agent()
{
	m_width = 10;
	m_height = 10;
}

Agent::Agent(float speed, Color colour) 
{
	m_width = 20;
	m_height = 20;

	SetData<Color>("colour", colour);
	SetData<float>("speed", speed);
}

Agent::~Agent()
{
	for (Behaviour* behaviour : m_behaviourList)
	{
		delete behaviour;
	}
}

void Agent::OffScreen(Agent* agent, int sw, int sh)
{
	Vector2 pos = agent->GetPosition();

	if (pos.y < 0)
		pos.y = sh;
	if (pos.y > sh)
		pos.y = 0;
	if (pos.x < 0)
		pos.x = sw;
	if (pos.x > sw)
		pos.x = 0;
	agent->SetPosition(pos);
}

Vector2 Agent::Truncate(Vector2 v, float max)
{
	float i = max / Vector2Length(v);
	i = i < 1.0 ? i : 1.0;
	return Vector2Scale(v, i);
}

// Update the agent and its behaviours
void Agent::Update(float deltaTime)
{
	for (unsigned int i = 0; i < m_behaviourList.size(); i++)
	{
		if (m_behaviourList[i]->Update(this, deltaTime) == Behaviour::eBehaviourResult::FAILURE)
		{
			throw "Processing a behaviour failed";
		}
	}	

	Vector2 velocity = GetVelocity();

	Vector2 translation = Vector2Scale(velocity, deltaTime);
	SetPosition(Vector2Add(GetPosition(), translation));
}

// Draw the agent
void Agent::Draw()
{
	DrawRectangle(m_position.x, m_position.y, m_width, m_height, BLUE);

	for (Behaviour* blist : m_behaviourList)
	{
		blist->Draw(this);
	}
}

// Add a behaviour to the agent
void Agent::AddBehaviour(Behaviour* behaviour)
{
	m_behaviourList.push_back(behaviour);
}
