#include "Player.h"
#include "KeyboardBehaviour.h"
#include "SteeringBehaviour.h"

Player::Player(float speed, Vector2 position, Color colour) 
	: Agent(speed, colour)
{
	SteeringBehaviour* steeringBehaviour = new SteeringBehaviour();
	steeringBehaviour->AddSteeringForce(new KeyboardBehaviour(speed));
	AddBehaviour(steeringBehaviour);

	SetPosition(position);
}