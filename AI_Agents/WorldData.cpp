#include "WorldData.h"

WorldData::WorldData()
{
}

WorldData::WorldData(const TileMap& tileMap)
	:tileMap(tileMap)
{
	CreateGraph(tileMap, 50, 50);
}

WorldData::WorldData(const TileMap& tileMap, unsigned int offsetX, unsigned int offsetY, unsigned int tileSizeX, unsigned int tileSizeY)
	: tileMap(tileMap)
{
	CreateGraph(tileMap, offsetX, offsetY, tileSizeX, tileSizeY);
}

//draw the sprites according to the tileType
void WorldData::Draw(Texture2D wall)
{
	for (Node* nodes : m_nodeList)
	{
		switch (nodes->tileType)
		{
		case TileType::GROUND:
			DrawRectangle((int)(nodes->position.x - m_tileSizeX / 2), (int)(nodes->position.y - m_tileSizeY / 2), m_tileSizeX, m_tileSizeY, GRAY);
			break;
		case TileType::WALL:
			DrawTexture(wall, (int)(nodes->position.x - m_tileSizeX / 2), (int)(nodes->position.y - m_tileSizeY / 2), WHITE);
			break;
		default:
			break;
		}
	}
#if _DEBUG
	NodeGraph::Draw();
#endif

}
