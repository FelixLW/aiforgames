#include "FiniteStateMachine.h"
#include "Transition.h"
#include "State.h"

//Behaviour::eBehaviourResult FiniteStateMachine::Update(Agent* agent, float deltaTime)
//{
//	if (m_stateChangeRequest != nullptr)
//	{
//		if (m_currentState != nullptr)
//			m_currentState->Exit(agent);
//
//		m_currentState = m_stateChangeRequest;
//		m_stateChangeRequest = nullptr;
//		m_currentState->Init(agent);
//	}
//
//	if (m_currentState != nullptr)
//	{
//		Transition* transition = m_currentState->GetTriggeredTransition(agent);
//
//		if (transition != nullptr)
//		{
//			m_currentState->Exit(agent);
//			m_currentState = transition->GetTargetState();
//			m_currentState->Init(agent);
//		}
//
//		m_currentState->Update(agent, deltaTime);
//		return eBehaviourResult::SUCCESS;
//	}
//	return eBehaviourResult::FAILURE;
//}

Behaviour::eBehaviourResult FiniteStateMachine::Update(Agent* agent, float deltaTime)
{
	if (m_currentState != nullptr)
	{
		Transition* transition = m_currentState->GetTriggeredTransition(agent);

		if (transition != nullptr)
		{
			m_currentState->Exit(agent);
			m_currentState = transition->GetTargetState();
			m_currentState->Init(agent);
		}

		m_currentState->Update(agent, deltaTime);
		return eBehaviourResult::SUCCESS;
	}
	return eBehaviourResult::FAILURE;
}