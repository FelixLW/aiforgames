#pragma once
#include "Behaviour.h"

class SteeringForce;

class SteeringBehaviour:
    public Behaviour
{
public:
    SteeringBehaviour();
    ~SteeringBehaviour();

    virtual eBehaviourResult Update(Agent* agent, float deltaTime) override;
    virtual void Draw(Agent* agent) override;

    void AddSteeringForce(SteeringForce* steeringForce);

    void SetMaxAcceleration(float maxAcceleration = FLT_MAX) { m_maxAcceleration = maxAcceleration; }
    float GetMaxAcceleration() const { return m_maxAcceleration; }

    void SetMass(float mass = 1) { m_mass = mass; }
    float GetMass() const { return m_mass; }

private:
    Vector2 Truncate(Vector2 v, float max);

protected:
    std::vector<SteeringForce*> m_steeringForces;
    float m_maxAcceleration = FLT_MAX;
    float m_mass = 1;
};

