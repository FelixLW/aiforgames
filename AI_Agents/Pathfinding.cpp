#include "PathFinding.h"
#pragma warning(push)
#pragma warning(disable: 26451)
#include "raymath.h"
#pragma warning(pop)
#include <algorithm>
#include <vector>
#include <iostream>
#include <sstream>
#include "BlackboardSingleton.h"
#pragma warning(disable: 6011)

float Heuristic(Node* target, Node* endNode)
{
    //return the distance between the currentNode and the goalNode
    Vector2 distanceBetweenTwoPoints = Vector2Subtract(target->position, endNode->position);
    return (sqrtf(distanceBetweenTwoPoints.x * distanceBetweenTwoPoints.x + distanceBetweenTwoPoints.y * distanceBetweenTwoPoints.y));

    //return(target->gScore - endNode->gScore);
}

bool NodeSort(Node* a, Node* b)
{
    //compares the fScore between two nodes -orginally was gScore
    return (a->fScore < b->fScore);
}

bool NodeSortDijkstra(Node* a, Node* b)
{
    return (a->gScore < b->gScore);
}

Path NodeGraph::AStarSearch(Node* startNode, Node* endNode)
{
    //validate the input, if we can't find the above nodes, throw an exception
    if (startNode == nullptr || endNode == nullptr)
    {
        throw "Both starting and end nodes are null";
    }

    if (startNode == endNode)
    {
        //return an empty path
        std::vector<Node*> singleNodePath;
        singleNodePath.push_back(startNode);
        return singleNodePath;
    }

    //initialise the starting node
    //both have to be intialised to get started on a dijkstra
    startNode->gScore = 0;
    startNode->parent = nullptr;

    //create a temporary lists for storing nodes we're visting/ have visited
    std::vector<Node*> openList;
    std::vector<Node*> closedList;

    //add startNode to openList
    openList.push_back(startNode);

    while (!openList.empty())
    {
        //sort the openlist by fScore
        std::sort(openList.begin(), openList.end(), NodeSort);  //doesn't need to take in any parameters?

        //Set the current node to the first node in the openList
        Node* currentNode = openList.front();

        if (currentNode == endNode)
        {
            break;
        }

        //remove currentNode from the openList
        openList.erase(openList.begin());

        //add currentNode to the closeList
        closedList.push_back(currentNode);

        for (Edge edges : currentNode->connections)
        {
            //If the target node is in the closedList, ignore it
            if (std::find(closedList.begin(), closedList.end(), edges.target) != closedList.end())
            {
                continue;
            }
            //If the target node is not in the openList, update it
            if (std::find(openList.begin(), openList.end(), edges.target) == openList.end())
            {
                //Calculate the target node's G Score
                edges.target->gScore = currentNode->gScore + edges.cost;

                //include a custom heuristic that takes a edges.target, endNode
                edges.target->hScore = Heuristic(edges.target, endNode);    //
                edges.target->fScore = edges.target->gScore + edges.target->hScore;

                //edges.target.fscore = fScore? //
                edges.target->fScore = currentNode->fScore;

                //Set the target node's previous to currentNode
                edges.target->parent = currentNode;

                //Find the earliest point we should insert the node
                //to the list to keep it sorted
                auto insertionPos = openList.end();
                for (auto i = openList.begin(); i != openList.end(); i++)
                {
                    if (edges.target->gScore < (*i)->gScore) {
                        insertionPos = i;
                        break;
                    }
                }

                //Insert the node at the appropriate position
                openList.insert(insertionPos, edges.target);
            }

            //the target node is in the open list otherwise
            else
            {
                if (currentNode->fScore < edges.target->fScore)
                {
                    edges.target->gScore = currentNode->gScore + edges.cost;
                    edges.target->fScore = currentNode->fScore;
                    edges.target->parent = currentNode;
                }
            }
        }
    }

    std::vector<Node*> path;
    Node* currentNode = endNode;

    while (currentNode != nullptr)
    {
        //push front the currentNode into the path list
        path.insert(path.begin(), currentNode);
        //go back to the previous node
        currentNode = currentNode->parent;
    }

    //return the path for navigation between startNode/endNode
    return path;
}

Path NodeGraph::DijkstraSearch(Node* startNode, Node* endNode)
{
    //validate the input, if we can't find the above nodes, throw an exception
    if (startNode == nullptr || endNode == nullptr)
    {
        throw "Both starting and end nodes are null";
    }

    if (startNode == endNode)
    {
        //return an empty path
        std::vector<Node*> singleNodePath;
        singleNodePath.push_back(startNode);
        return singleNodePath;
    }

    //initialise the starting node
    //both have to be intialised to get started on a dijkstra
    startNode->gScore = 0;
    startNode->parent = nullptr;

    //create a temporary lists for storing nodes we're visting/ have visited
    std::vector<Node*> openList;
    std::vector<Node*> closedList;

    //add startNode to openList
    openList.push_back(startNode);

    while (!openList.empty())
    {
        //sort the openlist by fScore
        std::sort(openList.begin(), openList.end(), NodeSortDijkstra);  //doesn't need to take in any parameters?

        //Set the current node to the first node in the openList
        Node* currentNode = openList.front();

        if (currentNode == endNode)
        {
            break;
        }

        //remove currentNode from the openList
        openList.erase(openList.begin());

        //add currentNode to the closeList
        closedList.push_back(currentNode);

        for (Edge edges : currentNode->connections)
        {
            //If the target node is in the closedList, ignore it
            if (std::find(closedList.begin(), closedList.end(), edges.target) != closedList.end())
            {
                continue;
            }
            //If the target node is not in the openList, update it
            if (std::find(openList.begin(), openList.end(), edges.target) == openList.end())
            {
                //Calculate the target node's G Score
                edges.target->gScore = currentNode->gScore + edges.cost;

                //Set the target node's previous to currentNode
                edges.target->parent = currentNode;

                //Find the earliest point we should insert the node
                //to the list to keep it sorted
                auto insertionPos = openList.end();
                for (auto i = openList.begin(); i != openList.end(); i++)
                {
                    if (edges.target->gScore < (*i)->gScore) {
                        insertionPos = i;
                        break;
                    }
                }

                //Insert the node at the appropriate position
                openList.insert(insertionPos, edges.target);
            }

            //the target node is in the open list otherwise
            else
            {
                if (currentNode->gScore + edges.cost < edges.target->gScore)    //not to add edges.cost for first part
                {
                    edges.target->gScore = currentNode->gScore + edges.cost;
                    edges.target->parent = currentNode;
                }
            }
        }
    }

    std::vector<Node*> path;
    Node* currentNode = endNode;

    while (currentNode != nullptr)
    {
        //push front the currentNode into the path list
        path.insert(path.begin(), currentNode);

        //go back to the previous node
        currentNode = currentNode->parent;
    }

    //return the path for navigation between startNode/endNode
    return path;
}

Path NodeGraph::DijkstraSearchID(const std::string& startNodeId, const std::string& endNodeId)
{
    //first find the nodes by Id
    Node* startNode = FindNodeId(startNodeId);
    Node* endNode = FindNodeId(endNodeId);

    //validate the input, if we can't find the above nodes, throw an exception
    if (startNode == nullptr || endNode == nullptr)
    {
        throw "Both starting and end nodes are null";
    }

    if (startNode == endNode)
    {
        //return an empty path
        std::vector<Node*> singleNodePath;
        singleNodePath.push_back(startNode);
        return singleNodePath;
    }

    //initialise the starting node
    //both have to be intialised to get started on a dijkstra
    startNode->gScore = 0;
    startNode->parent = nullptr;

    //create a temporary lists for storing nodes we're visting/ have visited
    std::vector<Node*> openList;
    std::vector<Node*> closedList;

    //add startNode to openList
    openList.push_back(startNode);

    while (!openList.empty())
    {
        //sort the openlist by fScore
        std::sort(openList.begin(), openList.end(), NodeSortDijkstra);  //doesn't need to take in any parameters?

        //Set the current node to the first node in the openList
        Node* currentNode = openList.front();

        if (currentNode == endNode)
        {
            break;
        }

        //remove currentNode from the openList
        openList.erase(openList.begin());

        //add currentNode to the closeList
        closedList.push_back(currentNode);

        for (Edge edges : currentNode->connections)
        {
            //If the target node is in the closedList, ignore it
            if (std::find(closedList.begin(), closedList.end(), edges.target) != closedList.end())
            {
                continue;
            }
            //If the target node is not in the openList, update it
            if (std::find(openList.begin(), openList.end(), edges.target) == openList.end())
            {
                //Calculate the target node's G Score
                edges.target->gScore = currentNode->gScore + edges.cost;

                //Set the target node's previous to currentNode
                edges.target->parent = currentNode;

                //Find the earliest point we should insert the node
                //to the list to keep it sorted
                auto insertionPos = openList.end();
                for (auto i = openList.begin(); i != openList.end(); i++)
                {
                    if (edges.target->gScore < (*i)->gScore) {
                        insertionPos = i;
                        break;
                    }
                }

                //Insert the node at the appropriate position
                openList.insert(insertionPos, edges.target);
            }

            //the target node is in the open list otherwise
            else
            {
                if (currentNode->gScore + edges.cost < edges.target->gScore)    //not to add edges.cost for first part
                {
                    edges.target->gScore = currentNode->gScore + edges.cost;
                    edges.target->parent = currentNode;
                }
            }
        }
    }

    std::vector<Node*> path;
    Node* currentNode = endNode;

    while (currentNode != nullptr)
    {
        //push front the currentNode into the path list
        path.insert(path.begin(), currentNode);
        //go back to the previous node
        currentNode = currentNode->parent;
    }

    //return the path for navigation between startNode/endNode
    return path;
}

void NodeGraph::DrawNode(Node* node, bool selected)
{
    static char buffer[10];
    sprintf_s(buffer, "%.0f", node->gScore);

    //draw an outline for the nodes
    DrawCircle((int)node->position.x, (int)node->position.y, 25, GREEN);

    //draw inner circle
    if (selected)
    {
        DrawCircle((int)node->position.x, (int)node->position.y, 22, GRAY);
    }
    else
    {
        DrawCircle((int)node->position.x, (int)node->position.y, 22, WHITE);
    }

    //draw the text
    DrawText(buffer, (int)node->position.x - 10, (int)node->position.y - 10, 15, RED);
}

void NodeGraph::DrawGraph(Node* node, std::vector<Node*>* drawnList)
{
    //once we draw a node, push it back into the list
    DrawNode(node);
    drawnList->push_back(node);

    for (Edge edges : node->connections)
    {
        //draw the connections of the nodes
        DrawLine((int)node->position.x, (int)node->position.y, (int)edges.target->position.x, (int)edges.target->position.y, BLACK);
        //Draw the cost
        Vector2 costPos = { (node->position.x + edges.target->position.x) / 2, (node->position.y + edges.target->position.y) / 2 };
        static char buffer[10];
        sprintf_s(buffer, "%.0f", edges.cost);
        DrawText(buffer, (int)costPos.x, (int)costPos.y, 15, BLACK);
        //draw the target node
        if (std::find(drawnList->begin(), drawnList->end(), edges.target) == drawnList->end())
        {
            DrawGraph(edges.target, drawnList);
        }
    }
}

void NodeGraph::Draw()
{
    DrawEdges();
    DrawNodes();
    DrawNodeIds();
}


void NodeGraph::AddNode(Node* node)
{
    m_nodeList.push_back(node);
}

void NodeGraph::DrawNode()
{
    for (Node* nodes : m_nodeList)
    {

        DrawCircle((int)nodes->position.x, (int)nodes->position.y, 20, BLUE);
    }
}

Node* NodeGraph::FindNodeId(const std::string& id)
{
    for (Node* nodes : m_nodeList)
    {
        if (nodes->id == id)
        {
            return nodes;
        }
    }
    return nullptr;
}

void NodeGraph::ConnectNodes(const std::string& NodeAId, const std::string& NodeBId, bool isTwoWay)
{
    float cost = Vector2Length(Vector2Subtract(GetPositionByNodeId(NodeAId), GetPositionByNodeId(NodeBId)));
    ConnectNodes(NodeAId, NodeBId, isTwoWay, cost);
}

void NodeGraph::ConnectNodes(const std::string& nodeAId, const std::string& nodeBId, bool isTwoWay, float cost)
{
    Node* nodeA = FindNodeId(nodeAId);
    Node* nodeB = FindNodeId(nodeBId);

    if (nodeA && nodeB)
    {
        nodeA->connections.push_back({ nodeB, cost });
    }

    //twoWay meaning a can go to b, and b can go to a
    if (isTwoWay)
    {
        nodeB->connections.push_back({ nodeA, cost });
    }
}

Vector2 NodeGraph::GetPositionByNodeId(const std::string& NodeId)
{
    Node* nodeA = FindNodeId(NodeId);

    return (nodeA != nullptr) ? nodeA->position : Vector2{ 0,0 };
}

void NodeGraph::DrawEdges()
{
    for (Node* nodes : m_nodeList)
    {
        for (Edge edge : nodes->connections)
        {
            //formating text 
            std::stringstream ss;
            ss << edge.cost;

            DrawLine((int)nodes->position.x, (int)nodes->position.y, (int)edge.target->position.x, (int)edge.target->position.y, BLACK);
            DrawText(ss.str().c_str(),
                (int)((nodes->position.x + edge.target->position.x) / 2.0f),
                (int)((nodes->position.y + edge.target->position.y) / 2.0f),
                10, BLUE);
        }
    }
}

void NodeGraph::DrawNodes()
{
    for (Node* nodes : m_nodeList)
    {
        DrawCircle((int)nodes->position.x, (int)nodes->position.y, 10, DARKBLUE);
    }
}

void NodeGraph::DrawNodeIds()
{
    //write each node Id
    for (Node* nodes : m_nodeList)
    {
        DrawText(nodes->id.c_str(), (int)(nodes->position.x), (int)(nodes->position.y), 10, BLACK);
    }
}

Node* NodeGraph::FindClosestNode(const Vector2& pos)
{
    float closestLengthSquared = FLT_MAX;    //maximum
    Node* closestNode = nullptr;

    for (Node* nodes : m_nodeList)
    {
        if (nodes->connections.size() > 0)
        {
            //calculate the magnitude of the vector ( distance)
            Vector2 directionToNode = Vector2Subtract(nodes->position, pos);
            float lengthSquared = Vector2DotProduct(directionToNode, directionToNode);

            if (lengthSquared < closestLengthSquared)
            {
                closestLengthSquared = lengthSquared;
                closestNode = nodes;
            }
        }
    }

    return closestNode;
}

Node* NodeGraph::GetNode(unsigned int index)
{
    if (index >= m_nodeList.size())
    {
        throw "Index is greater than the size of nodeList";
    }

    return m_nodeList[index];
}

void NodeGraph2D::CreateGraph(const TileMap& tiles, unsigned int offsetX, unsigned int offsetY, unsigned int tileSizeX, unsigned int tileSizeY)
{
    //offset from the screen

    m_tileSizeX = tileSizeX;
    m_tileSizeY = tileSizeY;

    //stores the width and height of 2d graph
    m_Height = tiles.size();
    m_Width = (m_Height > 0) ? tiles[0].size() : 0;

    //clears old nodes
    m_nodeList.clear();
    m_nodeList.reserve(m_Width * m_Height);

    //create nodes in the graph
    int nodeId = 0;
    for (unsigned int y = 0; y < m_Height; y++)
    {
        for (unsigned int x = 0; x < m_Width; x++)
        {
            //didn't have new, but since * added new keyword
            AddNode(new Node(nodeId++, { (float)(x * m_tileSizeX + offsetX), (float)(y * m_tileSizeY + offsetY) }, tiles[y][x]));
        }
    }

    //add edges using the input costs to the graph
    for (unsigned int y = 0; y < m_Height; y++)
    {
        for (unsigned int x = 0; x < m_Width; x++)
        {
            float cost = GetCost(tiles[y][x]);

            //connects the neighbouring nodes to our node
            //connect the nodes and check the cost
            if (cost > 0)
            {
                Node* currentNode = GetNode(x, y);;

                if (x > 0 && (GetCost(tiles[y][x - 1]) > 0))
                    GetNode(x - 1, y)->AddConnection(currentNode, cost);
                if (x < m_Width - 1 && (GetCost(tiles[y][x + 1]) > 0))
                    GetNode(x + 1, y)->AddConnection(currentNode, cost);
                if (y > 0 && (GetCost(tiles[y - 1][x]) > 0))
                    GetNode(x, y - 1)->AddConnection(currentNode, cost);
                if (y < m_Height - 1 && (GetCost(tiles[y + 1][x]) > 0))
                    GetNode(x, y + 1)->AddConnection(currentNode, cost);
            }
        }
    }

    //store m_nodeList in a blackBoard, so we can implement wall collision -only method that works
    BlackboardSingleton::Get()->SetData<Path>("listNode", m_nodeList);
}

Node* NodeGraph2D::GetNode(unsigned int x, unsigned int y)
{
    if ((x > m_Width) || (y > m_Height))
    {
        throw "Out of bounds";
    }
    //calulates the position of the node we want

    return NodeGraph::GetNode(y * m_Width + x);
}

float NodeGraph2D::GetCost(const TileType tileType)
{
    if ((int)tileType > 0)
    {
        return (float)tileType;
    }
    else
    {
        return 0;
    }
}

Node::Node()
{
}

Node::Node(const Vector2& pos)
    : position(pos)
{
}

Node::Node(const unsigned int id, const Vector2& pos, TileType tileType)
    : id(std::to_string(id)), position(pos), tileType(tileType)
{
}

Node::Node(const std::string& id, const Vector2& pos, TileType tileType)
    : id(id), position(pos), tileType(tileType)
{
}
