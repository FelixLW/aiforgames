#pragma once
#include "Behaviour.h"

class State;
class Transition;
class Condition;

class FiniteStateMachine :
	public Behaviour
{
public:
	FiniteStateMachine()
		: m_currentState(nullptr) {}

	virtual ~FiniteStateMachine() {
		for (auto state : m_states)
			delete state;
		for (auto transition : m_transitions)
			delete transition;
		for (auto condition : m_conditions)
			delete condition;
	}

	State* AddState(State* state) { m_states.push_back(state); return state; }

	Transition* AddTransition(Transition* transition) {
		m_transitions.push_back(transition);
		return transition;
	}

	Condition* AddCondition(Condition* condition) {
		m_conditions.push_back(condition);
		return condition;
	}

	void SetCurrentState(State* state) { m_currentState = state; }
	void RequestStateChange(State* state) { m_stateChangeRequest = state; }

	virtual eBehaviourResult Update(Agent* agent, float deltaTime) override;

protected:
	std::vector<State*>			m_states;
	std::vector<Transition*>	m_transitions;
	std::vector<Condition*>		m_conditions;

	State* m_currentState;
	State* m_stateChangeRequest = nullptr;
};

