#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "raymath.h"
#include "PathFinding.h"

template <typename T>
class Blackboard
{
public:
	T& GetEntry(const std::string& id);
	void SetEntry(const std::string& id, const T& value);
	bool HasEntry(const std::string& id);
	void Remove(const std::string& id);

protected:
	std::map<std::string, T>	m_data;
};

template<typename T>
T& Blackboard<T>::GetEntry(const std::string& id)
{
	//iter will be placed at same index as id
	auto iter = m_data.find(id);

	if (iter == m_data.end())
		throw "Cannot find data";

	return iter->second;
}

template<typename T>
void Blackboard<T>::SetEntry(const std::string& id, const T& value)
{
	//adding new data
	m_data[id] = value;
}

template<typename T>
bool Blackboard<T>::HasEntry(const std::string& id)
{
	return (m_data.find(id) != m_data.end());
}

template<typename T>
void Blackboard<T>::Remove(const std::string& id)
{
	auto iter = m_data.find(id);

	//if we found the data, remove it
	if (iter != m_data.end())
	{
		m_data.erase(iter);
	}
}

class BlackboardCollection
	: protected Blackboard<Vector2>,
	Blackboard<bool>,	
	Blackboard<int>,
	Blackboard<float>,
	Blackboard<Color>,
	Blackboard<Path>
{
public:

	template <class T>
	bool HasData(const std::string& id)
	{
		return (Blackboard<T>::HasEntry(id));
	}

	template <class T>
	T& GetData(const std::string& id)
	{
		return (Blackboard<T>::GetEntry(id));
	}

	template <class T>
	void SetData(const std::string& id, const T& value)
	{
		Blackboard<T>::SetEntry(id, value);
	}

	template <class T>
	void RemoveData(const std::string& id)
	{
		Blackboard<T>::Remove(id);
	}
};

