#pragma once
#include "Agent.h"
#include <string>
#include <vector>

class Enemy :
	public Agent
{
public:
	Enemy(Agent* target, float speed, Vector2 position, Color colour);
};

