#include "DebugBehaviour.h"

Vector2 DebugBehaviour::Update(Agent* agent, float deltaTime)
{
	return { 0, 0 };
}

void DebugBehaviour::Draw(Agent* agent)
{
#if _DEBUG
    //draws a line in front of the agent to show its velocity 
    DrawLine((int)(agent->GetPosition().x), (int)(agent->GetPosition().y),
        (int)(agent->GetPosition().x + (int)(agent->GetVelocity().x)),
        (int)(agent->GetPosition().y + (int)(agent->GetVelocity().y)),
        RED);

    DrawCircleLines((int)agent->GetPosition().x, (int)agent->GetPosition().y, (float)200, RED);

#endif
}
