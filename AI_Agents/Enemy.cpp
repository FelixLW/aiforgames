#include "Enemy.h"
#include "SteeringBehaviour.h"
#include "DebugBehaviour.h"
#include "AttackForce.h"
#include "FiniteStateMachine.h"
#include "State.h"
#include "Condition.h"
#include "Transition.h"

Enemy::Enemy(Agent* target, float speed, Vector2 position, Color colour)
    : Agent(speed, colour)
{
    // Finites State Machine
    FiniteStateMachine* fsm = new FiniteStateMachine();

    // The states
    AttackState* attackState = new AttackState();
    IdleState* idleState = new IdleState();

    // Conditions and their transitions
    WithinRangeCondition* withinRange = new WithinRangeCondition(this, target, 200);
    Transition* toAttackState = new Transition(attackState, withinRange);
    OutOfRangeCondition* outOfRange = new OutOfRangeCondition(this, target, 200);
    Transition* toIdleState = new Transition(idleState, outOfRange);

    // Add tranbsitions to the states
    idleState->AddTransition(toAttackState);
    attackState->AddTransition(toIdleState);

    // Add states to the FSM
    fsm->AddState(idleState);
    fsm->AddState(attackState); 
    fsm->AddCondition(withinRange);
    fsm->AddCondition(outOfRange);
    fsm->AddTransition(toAttackState);
    fsm->AddTransition(toIdleState);

    // Set the first state
    fsm->SetCurrentState(idleState);

    // Give the enemy the FSM
    AddBehaviour(fsm);

    SteeringBehaviour* enemyBehaviour = new SteeringBehaviour();

    enemyBehaviour->AddSteeringForce(new AttackForce(target));
    enemyBehaviour->AddSteeringForce(new DebugBehaviour());

    AddBehaviour(enemyBehaviour);

    SetData<bool>("attackEnabled", false);

	SetPosition(position);

    
}
