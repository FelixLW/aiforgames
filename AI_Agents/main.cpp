/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "Agent.h"
#include "PathFinding.h"
#include "Player.h"
#include "WorldData.h"
#include "BlackboardSingleton.h"
#include "Enemy.h"
#include <time.h>
#include <vector>

int main(int argc, char* argv[])
{
    // Initialization
    //--------------------------------------------------------------------------------------
    //int screenWidth = 800;
    //int screenHeight = 450;
    int screenWidth = 1280;
    int screenHeight = 768;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    NodeGraph* nodeGraph = new NodeGraph();

    TileMap tileMap = { { TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND },
                    { TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::WALL, TileType::WALL, TileType::WALL, TileType::WALL, TileType::GROUND },
                    {TileType::WALL, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND},
                    {TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND},
                    {TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND},
                    {TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::WALL, TileType::WALL, TileType::WALL, TileType::WALL, TileType::GROUND},
                    {TileType::GROUND, TileType::WALL, TileType::WALL, TileType::WALL, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::GROUND},
                    {TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::GROUND},
                    {TileType::WALL, TileType::WALL, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::GROUND, TileType::WALL, TileType::WALL, TileType::GROUND, TileType::WALL, TileType::WALL, TileType::GROUND}
    };
    unsigned int sizeX = 68;
    unsigned int sizeY = 48;
    WorldData worldData(tileMap, 25, 45, sizeX, sizeY);  //magic numbers are offset

    std::cout << worldData.TileMapSize() << std::endl;



















    std::vector<Agent*> agents;

    Player* player = new Player(150.0f, Vector2{ (float)screenWidth - 100, (float)screenHeight - 100}, RED);
    agents.push_back(player);
    
    Enemy* enemy = new Enemy(player, 100, Vector2{ 300, 300 }, RED);
    agents.push_back(enemy);










    float deltaTime = 0;
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------
        deltaTime = GetFrameTime();

        for (Agent* agent : agents) {
            agent->Update(deltaTime);
        }


        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        for (Agent* agent : agents) 
        {
            agent->Draw();
        }

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    for (Agent* agent : agents) {
        delete agent;
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------   
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}