#pragma once
#include "SteeringForce.h"
#include "Agent.h"

class AttackForce :
	public SteeringForce
{
public:

    AttackForce()
    {
        m_target = nullptr;
    }
    AttackForce(Agent* target)
    {
        m_target = target;
    }
    ~AttackForce() {};

    virtual Vector2 Update(Agent* agent, float deltaTime);

private:
    Agent* m_target;
};

