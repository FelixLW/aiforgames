#pragma once
#include "Agent.h"

class Behaviour
{
public:

	enum class eBehaviourResult
	{
		SUCCESS,
		FAILURE
	};

	// empty constructors and destructors for base class
	Behaviour() {}
	virtual ~Behaviour() {}

	// pure virtual function for executing the behaviour
	virtual eBehaviourResult Update(Agent* agent, float deltaTime) { return eBehaviourResult::SUCCESS; };

	virtual void Draw(Agent* agent) {};
};
