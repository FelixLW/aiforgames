#pragma once
#include "raymath.h"

class Agent;

class SteeringForce
{
public:
	virtual ~SteeringForce() {}

	virtual Vector2 Update(Agent* agent, float deltaTime) = 0;

	virtual void Draw(Agent* agent) {}

	void SetAcceleration(float acceleration) { m_SteeringAcceleration = acceleration; }
	float GetAcceleration() const { return m_SteeringAcceleration; }

	void SetWeighting(float weighting) { m_Weighting = weighting; }
	float GetWeighting() const { return m_Weighting; }

protected:
	float m_SteeringAcceleration = 1000;
	float m_Weighting = 1;
};

