#pragma once
#include "Agent.h"

class AgentArrive :
	public Agent
{
public:
	AgentArrive();
	~AgentArrive();

	// Draw the agent
	virtual void Draw(Color colour) override;

	virtual void SetMaxSpeed(float speed) override { m_maxSpeed = speed; }
	virtual float GetMaxSpeed() override { return m_maxSpeed; }

	virtual void SetColour(Color colour) override { m_colour = colour; }
	virtual Color GetColour() override { return m_colour; }

protected:
	float m_maxSpeed = 100;
	Color m_colour = MAROON;
};