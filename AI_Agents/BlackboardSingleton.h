#pragma once
#include "Blackboard.h"

class BlackboardSingleton
	: public BlackboardCollection
{
public:
	//make a copy constructor to ensure we don't make a copy of a singleton and instead use a reference of it
	BlackboardSingleton(const BlackboardSingleton&) = delete;

	//make static so we can access this class
	static BlackboardSingleton* Get()
	{
		//returns the global instance of this class
		return s_Instance;
	}

private:
	//a constructor here prevents instantiation
	BlackboardSingleton() {}

	static BlackboardSingleton* s_Instance;
};

