#pragma once
#include <vector>
#include "agent.h"
#include "SteeringForce.h"

class Agent;
class Transition;

class State
{
public:
	State() {}
	virtual ~State();

	virtual void Init(Agent* agent) {}
	virtual void Update(Agent* agent, float deltaTime) = 0;
	virtual void Exit(Agent* agent) {}

	void AddTransition(Transition* transition);

	Transition* GetTriggeredTransition(Agent* agent);

protected:
	std::vector<Transition*> m_transitions;
};

class IdleState
	: public State
{
public:
	IdleState() {}
	virtual ~IdleState() {}

	virtual void Init(Agent* agent) override;
	virtual void Update(Agent* agent, float deltaTime) override;
};

class WanderState
	: public State
{
public:
	WanderState() {}
	virtual ~WanderState() {}

	virtual void Init(Agent* agent) override;
	virtual void Update(Agent* agent, float deltaTime) override;
};

class AttackState
	: public State
{
public:
	AttackState() {}
	virtual ~AttackState() {}

	virtual void Init(Agent* agent) override;
	virtual void Update(Agent* agent, float deltaTime) override;
	
};

