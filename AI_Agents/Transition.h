#pragma once
#include "Condition.h"

class State;
class Agent;

class Transition
{
public:
	Transition(State* target, Condition* condition)
		: m_target(target), m_condition(condition) {}

	~Transition() {}

	State* GetTargetState() { return m_target; }

	bool HasTriggered(Agent* agent) { return m_condition->Test(agent); }

private:
	State*		m_target;
	Condition*	m_condition;
};

